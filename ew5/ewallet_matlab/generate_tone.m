clear all; close all; clc;
singen_simulation='..\ewallet_simulations\x64\Debug\tools_demo.exe';
db_folder='..\ewallet_db';

if not(isfolder(db_folder))
    mkdir(db_folder)
end

command=[singen_simulation, ' 0', [' ',db_folder,'\stereo_out.wav'], ' 1'];
system(command)

[sig1, Fs1]=audioread([db_folder,'\stereo_out.wav']);
f=-Fs1/2:Fs1/length(sig1):Fs1/2-Fs1/length(sig1);

figure
plot(sig1(:,1),sig1(:,2))
hold on
plot(sig1(:,1),sig1(:,2),'.r')

figure(2)
hold on
plot(f,abs(fftshift(fft(sig1(:,1)))))
plot(f,abs(fftshift(fft(sig1(:,2)))))
grid on




