clear all; close all; clc;
singen_simulation='..\ewallet_simulations\x64\Debug\tools_demo.exe';
db_folder='..\ewallet_db';

FFT_SIZE=4096;
Fs=16000;
fsig=1234;


if not(isfolder(db_folder))
    mkdir(db_folder)
end

stereo_in=split_complex_to_vectors(complex_tone(fsig, Fs));
audiowrite([db_folder,'\stereo_in.wav'],stereo_in,Fs);
[stereo_in, Fs]=audioread([db_folder,'\stereo_in.wav']);
f=-Fs/2:Fs/length(stereo_in):Fs/2-Fs/length(stereo_in);
figure
plot(f,fftshift(abs(fft(stereo_in))))
title("Input wave spectrum");
xlabel('Frequency [Hz]')

% Run C Simulation

command=[singen_simulation,[' ',db_folder,'\stereo_in.wav'] , [' ',db_folder,'\stereo_out.wav'], ' 2'];
system(command)

% Analyze and plot C simulation results

[SIG_FFT, Fs]=audioread([db_folder,'\stereo_out.wav']);
SIG_FFT=SIG_FFT(1:FFT_SIZE,1)+1i*SIG_FFT(1:FFT_SIZE,2);
f=-Fs/2:Fs/length(SIG_FFT):Fs/2-Fs/length(SIG_FFT);
figure
stem(f,fftshift(abs(SIG_FFT)))
title("Analysis result spectrum");
xlabel('Frequency [Hz]')
[~, ind]=max(abs(SIG_FFT(1:FFT_SIZE/2)));
fprintf('Genereted %f max freq found=%f\n', fsig, f(ind+FFT_SIZE/2));




