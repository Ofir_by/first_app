clc 
clear all
close all
delay = 1331;
signal = create_signal();
X_recieved = recieve_signal(delay,signal);
figure
plot(X_recieved)
est_del=find_delay(X_recieved,signal)
est_del2=find_delay_fft(X_recieved,signal)







function X = create_signal()
    N=4096;
    Fs=16000;
    X = gausswin(N);
    ch=chirp((1:N)/Fs,1000,N/(2*Fs),2000,'quadratic');
   %% X=X.*ch';  %%here you can add a chirp
end

function X = recieve_signal(delay,signal) 
    N = 4096;
    X = [zeros(delay,1);signal;zeros(N-delay,1)]; 
   %% X= awgn(X,30);   %%here you can add the awgn
end

function ind = find_delay(received_signal,signal)
    [amp,n]=xcorr(received_signal,signal);
    figure
    plot(n,abs(amp));
    [~,i]=max(abs(amp));
    ind = n(i);
end


function ind = find_delay_fft(received_signal,signal)
    a=0;
    N=4096;
    signal2=[signal;zeros(3*N,1)];
    received_signal2=[received_signal;zeros(2*N,1)];
    rec_fft=fftshift(fft(received_signal2));
    sig_fft=fftshift(fft(signal2));
    amp=fftshift(ifft((rec_fft.*conj(sig_fft))));
    n=-2*N:(2*N-1);
    figure
    plot(n,abs(amp));
    [~,i]=max(abs(amp));
    ind = n(i);
end
