% Generate complex tone

function [stereo_in]=complex_tone(fsig, Fs)
    n=0:2*Fs-1;
    deltaphase=2*pi*fsig/Fs;
    phi=n*deltaphase;
    phi=reshape(phi,[],1);
    phi=mod(phi,2*pi);
    stereo_in=0.5*cos(phi);
    stereo_in=stereo_in +1i*0.5*sin(phi);
end


