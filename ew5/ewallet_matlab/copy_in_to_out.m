clear all; close all; clc;
%singen_simulation='..\ewallet_simulations\Debug\tools_demo.exe';
singen_simulation='..\ewallet_simulations\x64\Debug\tools_demo.exe';
db_folder='..\ewallet_db';

if not(isfolder(db_folder))
    mkdir(db_folder)
end

Fs=16000;
n=0:10*Fs-1;
fsig=1000;
deltaphase=2*pi*fsig/Fs;
phi=n*deltaphase;
phi=reshape(phi,[],1);
phi=mod(phi,2*pi);
stereo_in=0.5*cos(phi)+0.5*cos(4*phi)+0.5*cos(3*phi);
stereo_in(:,2)=0.5*sin(phi);
plot(stereo_in)
f=-Fs/2:Fs/length(stereo_in):Fs/2-Fs/length(stereo_in);
figure
plot(f,fftshift(abs(fft(stereo_in))))
audiowrite([db_folder,'\stereo_in.wav'],stereo_in,Fs);

command=[singen_simulation, [' ',db_folder,'\stereo_in.wav'], [' ',db_folder,'\stereo_out.wav'], ' 0'];
system(command)

[stereo_out, Fs1]=audioread([db_folder,'\stereo_out.wav']);
f=-Fs1/2:Fs1/length(stereo_out):Fs1/2-Fs1/length(stereo_out);
figure
plot(f,fftshift(abs(fft(stereo_out))))

len=min(length(stereo_in),length(stereo_out));

max_inout_diff=max(abs(stereo_in(1:len,:)-stereo_out(1:len,:)));
fprintf('Max in/out diff (1)= %2.1e \nMax in/out diff (2)= %2.1e \n', max_inout_diff);








