function [v_out] = split_complex_to_vectors(c_in)
    v_out(:,1)=real(c_in);
    v_out(:,2)=imag(c_in);
end

