 clear all; close all; clc;
singen_simulation='..\ewallet_simulations\x64\Debug\tools_demo.exe';
db_folder='..\ewallet_db';

FFT_SIZE=4096;
Fs=16000;
fsig=250;


if not(isfolder(db_folder))
    mkdir(db_folder)
end
%%
% stereo_in1=  split_complex_to_vectors(complex_tone(1000, Fs))+....
%              split_complex_to_vectors(complex_tone(1750, Fs))+...
%              split_complex_to_vectors(complex_tone(2250, Fs))+...
%              split_complex_to_vectors(complex_tone(2625, Fs));
stereo_in2=  split_complex_to_vectors(complex_tone(1000, Fs));%+....
%              split_complex_to_vectors(complex_tone(1750, Fs))+...
%              split_complex_to_vectors(complex_tone(2250, Fs))+...
%              split_complex_to_vectors(complex_tone(2750, Fs));

stereo_in=stereo_in2;
%stereo_in(1001:end,:)=stereo_in2(1001:end,:);
stereo_in=stereo_in/max(abs(stereo_in));  % added by Alon to prevent clipping whe writing to WAV

audiowrite([db_folder,'\stereo_in.wav'],stereo_in,Fs);
[stereo_in, Fs]=audioread([db_folder,'\stereo_in.wav']);
f=-Fs/2:Fs/length(stereo_in):Fs/2-Fs/length(stereo_in);
% figure
% plot(f,fftshift(abs(fft(stereo_in))))
% title("Input wave spectrum");
% xlabel('Frequency [Hz]') 

% Run C Simulation


command=[singen_simulation,[' ',db_folder,'\stereo_in.wav'] , [' ',db_folder,'\stereo_out.wav'], ' 3'];
system(command)


%%
[SIG_FFT, Fs1]=audioread([db_folder,'\stereo_out.wav']);
SIG_FFT=SIG_FFT(1:FFT_SIZE,:);
f=-Fs1/2:Fs1/length(SIG_FFT):Fs1/2-Fs1/length(SIG_FFT);
%f=0:Fs1/length(SIG_FFT):Fs1-1/Fs1/length(SIG_FFT);

figure
hold on
plot(f,(abs(fftshift((SIG_FFT)))))          % (SIG_FFT(:,1).^2+SIG_FFT(:,2).^2)))
%plot(f,fftshift(abs(fft(stereo_in(1:4096)))))
%plot(f,(SIG_FFT))s
xlabel('Frequency [Hz]')
hold off

figure
plot(1:32000,stereo_in )


