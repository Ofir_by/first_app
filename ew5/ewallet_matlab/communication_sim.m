clear all; close all; clc;
singen_simulation='..\ewallet_simulations\x64\Debug\tools_demo.exe';
db_folder='..\ewallet_db';
Fs=16000;
if not(isfolder(db_folder))
    mkdir(db_folder)
end
FFT_SIZE=4096;
%% Code Message
close all;
message_size=3;
stereo_in= zeros(2*FFT_SIZE*message_size, 2);
audiowrite([db_folder,'\stereo_in.wav'],stereo_in,Fs);

command=[singen_simulation,[' ',db_folder,'\stereo_in.wav'] , [' ',db_folder,'\stereo_out.wav'], ' 8', ' omg'];
system(command)
stereo_out= (audioread([db_folder,'\stereo_out.wav']));
% f=-Fs/2:Fs/length(stereo_out):Fs/2-Fs/length(stereo_out);
 figure()
 plot(stereo_out(:,1))
 title('Signal of message');
% figure()
% plot(f,fftshift(fft(stereo_out(:,1))));
%% Channel

noised_stereo= awgn(stereo_out,100);
noised_stereo(:,1)= noised_stereo(:,1)/max(abs(noised_stereo(:,1)));
noised_stereo(:,2)= noised_stereo(:,2)/max(abs(noised_stereo(:,2)));
%noised_stereo=stereo_out;
figure
plot(noised_stereo(:,1))
%% Decode Message

audiowrite([db_folder,'\stereo_in.wav'],noised_stereo,Fs);
command=[singen_simulation,[' ',db_folder,'\stereo_in.wav'] , [' ',db_folder,'\stereo_out.wav'], ' 9',' chg'];
system(command)

%%
stereo_in= [0.5*ones(2, FFT_SIZE) 0.4*ones(2,FFT_SIZE) 0.3*ones(2,FFT_SIZE) 0.2*ones(2, FFT_SIZE)]';
audiowrite([db_folder,'\stereo_in.wav'],stereo_in,Fs);
command=[singen_simulation,[' ',db_folder,'\stereo_in.wav'] , [' ',db_folder,'\stereo_out.wav'], ' 10', ' b'];
system(command)