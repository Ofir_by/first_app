// ewallet_simulations.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//#include <iostream>
//
//int main()
//{
//    std::cout << "Hello World!\n";
//}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "..\..\app\src\main\cpp\parameters.h"
#include "..\..\app\src\main\cpp\signal-processing.h"
#include "..\..\app\src\main\cpp\fft.h"
#include "read_audio_samples.h"
#include "..\..\app\src\main\cpp\fft.h"
#include <string>
#include "WAV_Writer.h"
using std::string;


/*------- Console (PC) Variables Decleration ---------*/
// Console (PC) app variables
short int* ptr = NULL;
//short int* ptr;

int bufpointer;
int count;
int retval = -1;
int Segment = 1;

static double Y[FFT_SIZE][2];


/*------- Meter variables --------------------------*/
FILE* fout;
size_t ws;
errno_t err;
char wavfilename[64] = { 0 };
short int status;
int NumberOfSamples;
short int audioDataIn[2 * BLOCK_SIZE], SamplesOut[2 * BLOCK_SIZE] = { 0 }, short_temp;
int SamplesOutPtr = 0;
int msg_index = 0;
double proc_time;
bool inputfile = false, reading = false;
char a = 'h';
double SA[FFT_SIZE][2] = { 0 };
double curr_max_tone = 0;
double temp = 0;
int mode;
int i = 0;
short SSA[BLOCK_SIZE * 2] = { 0 },  two_blocks[BLOCK_SIZE * 4] = { 0 };
double tones[DETECTED_TONES] = { 0 };
int freq_index = 256;
std :: string detected_message=" ", message=" ", start="(", end=")";
int  slice;
char received_letters[FFT_SIZE / MIN_FFT_SIZE] = { 0 };
char majority_letter = ' ', start_char= start[0], end_char=end[0], space=' ';



int main(int argc, char* argv[]) {
	int j;


	if (argc < 4) {
		printf("Not enough arguments\n");
		printf("Usage  : singen_simulation <Input stereo WAV filename> <Output stereo WAV filename> <processing_mode>\n");
		return (EXIT_FAILURE);
	}
	
	sscanf_s(argv[3], "%d", &mode);	
	if (argv[4] != NULL) {
		message = argv[4];
		message = start + message + end;
		printf("\n message: %s \n", message.c_str());
	}
	
	printf("Modes:\n 0 - LOOPBACK \n 1 - SINGEN\n 2 - SPECTRAL ANALYSIS \n 3 - SPECTRAL ANALYSIS & TONE DETECTION\n");
	printf("4 - INVERSE FFT \n 5 - CODE\n 6 - DECODE \n 7 - MAJORITY VOTING \n  8 - ENCODE MESSAGE \n 9 - DECODE MESSAGE \n 10 - INPUT INSERTION");
	printf("\nProcessing Mode %d \n", mode);

	int n = sprintf_s(wavfilename, 64, "%s", argv[2]);
	
	err = fopen_s(&fout, wavfilename, "wb");
	if (err == 0) {
		printf("Output file '%s' opened succeesfully\n", argv[2]);
	}
	else {
		printf("Couldn't open Output file '%s'\n", argv[2]);
		return (EXIT_FAILURE);
	}
	
	/* ---------------read input file ------------------*/
	ptr = read_audio_samples(argv[1], &count);


	if (ptr == NULL) {
		printf("No input file '%s' \n", argv[1]);
		inputfile = false;
	}
	else {
		printf("input file '%s' opened succeesfully\n", argv[1]);
		inputfile = true;
	}



	// ---------------Initialize ---------------

	fft_init(MAX_FFT_SIZE);

	if (inputfile) {
		NumberOfSamples = (int)(floor(((double)count) / (2.0 * BLOCK_SIZE)) * (2.0 * BLOCK_SIZE));
	//	printf("\n> size: %d  numofSamps: %d", count, NumberOfSamples);
	}
	else
	{
		NumberOfSamples = 2 * SAMPLING_FREQ * 2;
	}


	
	status = WriteWavePCM_Stereo_Header(NumberOfSamples, fout);
	bufpointer = 0;

	while (bufpointer < NumberOfSamples- (2 * BLOCK_SIZE)) {

		for (j = 0; j < BLOCK_SIZE; j++) {
			SA[j][0] = 0;
			SA[j][1] = 0;
		}
	//	printf("\n> block %d ", bufpointer /(2 * BLOCK_SIZE));
	//	printf("\n> bufferpnt %d  out of %d ", bufpointer, NumberOfSamples - (2 * BLOCK_SIZE));
		switch (mode) {
		/* copy input wav to output wav */
		case LOOPBACK:
			if (inputfile) {
				for (j = 0; j < 2 * BLOCK_SIZE; j++) {
					audioDataIn[j] = ptr[bufpointer + j];
					SamplesOut[j] = audioDataIn[j];
				}
			}
			break;

		/* create sine waves */
		case SINGEN:
			//singen(SamplesOut, 2 * BLOCK_SIZE, 200, 100, SAMPLING_FREQ);
			singen_from_table(SamplesOut, 2 * BLOCK_SIZE, 2500, SAMPLING_FREQ);
			break;

		/* spectral analysis of one channel*/
		case SPECTRAL_ANALYSIS:
			/*for (j = 0; j < BLOCK_SIZE; j++) {
				short_temp = ptr[bufpointer + (2 * j)];
				SA[j][0] = (double)short_temp;
				SA[j][1] = 0;
			}*/

			singen_from_table(SamplesOut, 2 * BLOCK_SIZE, 2500, SAMPLING_FREQ);
			for (j = 0; j < BLOCK_SIZE; j++) {
				
				SA[j][0] = (double)SamplesOut[2*j];
				SA[j][1] = 0;
			}
			gpfft(SA, 0);
			
			for (j = 0; j < BLOCK_SIZE; j++) {
				if (SA[j][0] != 0) {
					printf("\n SA[%d][0] = % f", j, SA[j][0]);
				}
				SamplesOut[2 * j] = (short)SA[j][0];
				SamplesOut[2 * j + 1] = (short)SA[j][1];
				

			}
			break;

		/* finding the tone with the largest amplitude*/
		case TONE_DETECTION:
			
			for (j = 0; j < BLOCK_SIZE; j++) {
				SSA[2*j] = ptr[bufpointer + (2 * j)];
				SSA[2*j+1] = ptr[bufpointer + (2 * j + 1)];
			}


			tone_detection(SSA, tones,0);
			for (i = 0;i < DETECTED_TONES;i++) {
				printf("\n max tone number %d is %f", i + 1, tones[i]);
			}
			break;

			/* finding the tone with the largest amplitude*/
		case INV_FFT:
			SA[freq_index][0] = FFT_SIZE / 2;
			SA[FFT_SIZE - freq_index][0] = FFT_SIZE / 2;
			
			
			//printf("\n SA[256][0] = %f", SA[freq_index][0]);

			ifft(SA, 0);
			
			for (j = 0; j < BLOCK_SIZE; j++) {
				
				
				//printf("\n SA[%d][0] = % f", j, SA[j][0]);
				SamplesOut[2 * j] = (short)(SA[j][0]*32767);
				SamplesOut[2 * j + 1] = (short)(SA[j][1]*32767);
				printf("\n SamplesOut[2*%d] = %d", j, SamplesOut[2*j]);
			}
			printf("\n SA[%d][0] = % f", 256, SA[256][0]);
			printf("\n SA[%d][0] = % f", 3840, SA[3840][0]);
			break;
		case CODE:
			
			convert_ascii_to_tone(SA, a);


			ifft(SA, 0);
			for (j = 0; j < BLOCK_SIZE; j++) {


				//printf("\n SA[%d][0] = % f", j, SA[j][0]);
				SamplesOut[2 * j] = (short)(SA[j][0] * 32767);
				SamplesOut[2 * j + 1] = (short)(SA[j][1] * 32767);
				//printf("\n SamplesOut[2*%d] = %d", j, SamplesOut[2 * j]);
			}
			//printf("\n SA[%d][0] = % f", 256, SA[256][0]);
			//printf("\n SA[%d][0] = % f", 3840, SA[3840][0]);
			break;
		case DECODE:

			convert_ascii_to_tone(SA, a);
			
			for (i = 0;i < FFT_SIZE;i++) {
				SSA[2*i] = (short)(Q14*(SA[i][0]));
				SSA[2*i+1] = (short)(Q14*(SA[i][1]));
				SamplesOut[2 * i] = (short)(SA[i][0] * 32767);
				SamplesOut[2 * i + 1] = (short)(SA[i][1] * 32767);
				//printf("\n ssa=%d", SSA[2 * i]);
			}
			for (slice = 0;slice < FFT_SIZE / MIN_FFT_SIZE;slice++) {
				tone_detection(SSA, tones, slice);
				received_letters[slice] = convert_tone_to_ascii(tones);
				printf("\nreceived_letters[%d]=%c", slice, received_letters[slice]);
			}

			for (i = 0;i < DETECTED_TONES;i++) {
				printf("\n max tone number %d is %f", i + 1, tones[i]);
			}
			a=convert_tone_to_ascii(tones);
			printf("\n the char detected is %c", a);
			
			break;
		case MAJORITY_VOTING:
			

			for (j = 0; j < BLOCK_SIZE; j++) {
				SSA[2 * j] = ptr[bufpointer + (2 * j)];
				SSA[2 * j + 1] = 0;
			}

			for (slice = 0;slice < FFT_SIZE / MIN_FFT_SIZE;slice++) {
				tone_detection(SSA, tones, slice);
				received_letters[slice] = convert_tone_to_ascii(tones);
				printf("\nreceived_letters[%d]=%c", slice, received_letters[slice]);
			}

			for (i = 0;i < DETECTED_TONES;i++) {
				printf("\n max tone number %d is %f", i + 1, tones[i]);
			}
			a = majority_voting(received_letters);
			printf("\n the char detected is %c", a);

			break;

		case ENCODE_MESSAGE:
			
			if (msg_index < message.size()) {
				printf("\n message[%d]: % c ", msg_index, message[msg_index]);
				convert_ascii_to_tone(SA, message[msg_index]);
			
				for (j = 0; j < BLOCK_SIZE; j++) {

					SamplesOut[2 * j] = (short)(SA[j][0] * 32767);
					SamplesOut[2 * j + 1] = (short)(SA[j][1] * 32767);
				}
			}
			else
			{
				for (j = 0; j < BLOCK_SIZE; j++) {
					SamplesOut[2 * j] = 0;
					SamplesOut[2 * j + 1] = 0;
				}
			}

			break;

		case DECODE_MESSAGE:
			
			for (j = 0; j < BLOCK_SIZE; j++) {
				SSA[2 * j] = ptr[bufpointer + (2 * j)];
				SSA[2 * j + 1] = ptr[bufpointer + (2 * j + 1)];
			}
			
			for (slice = 0; slice < FFT_SIZE / MIN_FFT_SIZE; slice++) {
				tone_detection(SSA, tones, slice);
				if (tones[1] > -200) {
					
					received_letters[slice] = convert_tone_to_ascii(tones);
					//printf("\n received_letters[%d]=%c", slice, received_letters[slice]);
				}
				else
				{
					received_letters[slice] = { ' ' };
				}

				//
			}
			
			majority_letter = majority_voting(received_letters);
			if (majority_letter == start_char && reading == false)
			{
				reading = true;
				printf("\n The message detected: \n");
			}
			else if (reading == true)
			{
				if (majority_letter == end_char)
				{
					reading = false;
					printf("\n End of message.");
				}
				else
				{
					if (majority_letter == '_') {
						majority_letter = space;}
					printf("%c", majority_letter);
				}
				
			}
			
			//for (i = 0; i < DETECTED_TONES; i++) {
			//	printf("\n max tone number %d is %f", i + 1, tones[i]);
			//}
			
			//detected_message +=majority_letter;
			

		break;

		case INPUT_INSERTION:
			for (j = 0; j < BLOCK_SIZE; j++) {
				SSA[2 * j] = ptr[bufpointer + (2 * j)];
				SSA[2 * j + 1] = ptr[bufpointer + (2 * j + 1)];
			}
			
			input_insertion(two_blocks, SSA);

			printf("\n first half: %d",two_blocks[4]);
			printf("\n second half: %d", two_blocks[2*BLOCK_SIZE+4]);
			printf("\n");
		break;
		/*******/
		default:
			printf("\n mode %d not supported", mode);
			break;
		}


		msg_index++;
		ws = fwrite(SamplesOut, sizeof(short), 2 * BLOCK_SIZE, fout);
		bufpointer += 2 * BLOCK_SIZE;
	}

	fclose(fout);

	printf("\n******singen_simulation end******\n");


	return (0);
} /*end main*/
