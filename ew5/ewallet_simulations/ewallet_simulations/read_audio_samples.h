#ifndef READAUDIOSAMPLES_H
#define READAUDIOSAMPLES_H

short int * read_audio_samples(const char *filename, int  count[]);

#endif