//#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "..\..\app\src\main\cpp\parameters.h"


const char fChunkID[] = { 'R', 'I', 'F', 'F' };
const char fFormat[] = { 'W', 'A', 'V', 'E' };
const char fSubchunk1ID[] = { 'f', 'm', 't', ' ' };
const char fSubchunk2ID[] = { 'd', 'a', 't', 'a' };



const unsigned short	N_CHANNELS = 2;
const unsigned int		SAMPLE_RATE = SAMPLING_FREQ;
const unsigned short	BITS_PER_BYTE = 8;






short int WriteWavePCM_Stereo_Header(size_t pairAmount, FILE* fout) {
	const static unsigned int fSubchunk1Size = 16;
	const static unsigned short fAudioFormat = 1;
	const static unsigned short fBitsPerSample = 16;

	unsigned int fByteRate = SAMPLE_RATE * N_CHANNELS *
		fBitsPerSample / BITS_PER_BYTE;

	unsigned short fBlockAlign = N_CHANNELS * fBitsPerSample / BITS_PER_BYTE;
	unsigned int fSubchunk2Size;
	unsigned int fChunkSize;


	fSubchunk2Size = pairAmount * N_CHANNELS * fBitsPerSample / BITS_PER_BYTE;
	fChunkSize = 36 + fSubchunk2Size;

	// Writing the RIFF header:
	fwrite(&fChunkID, 1, sizeof(fChunkID), fout);
	fwrite(&fChunkSize, sizeof(fChunkSize), 1, fout);
	fwrite(&fFormat, 1, sizeof(fFormat), fout);
	// "fmt" chunk:
	fwrite(&fSubchunk1ID, 1, sizeof(fSubchunk1ID), fout);
	fwrite(&fSubchunk1Size, sizeof(fSubchunk1Size), 1, fout);
	fwrite(&fAudioFormat, sizeof(fAudioFormat), 1, fout);
	fwrite(&N_CHANNELS, sizeof(N_CHANNELS), 1, fout);
	fwrite(&SAMPLE_RATE, sizeof(SAMPLE_RATE), 1, fout);
	fwrite(&fByteRate, sizeof(fByteRate), 1, fout);
	fwrite(&fBlockAlign, sizeof(fBlockAlign), 1, fout);
	fwrite(&fBitsPerSample, sizeof(fBitsPerSample), 1, fout);

	/* "data" chunk: */
	fwrite(&fSubchunk2ID, 1, sizeof(fSubchunk2ID), fout);
	fwrite(&fSubchunk2Size, sizeof(fSubchunk2Size), 1, fout);

	return 1;
}









