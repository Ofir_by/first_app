//#include <iostream>
#include <stdio.h>
#include <stdlib.h>
//using namespace std;
 
#define BUFSIZE  256                 // BUFSIZE can be changed according to the frame size required (eg:512)

typedef struct header_file

{
    char chunk_id[4];
    int chunk_size;
    char format[4];
    char subchunk1_id[4];
    int subchunk1_size;
    short int audio_format;
    short int num_channels;
    int sample_rate;            // sample_rate denotes the sampling rate.
    int byte_rate;
    short int block_align;
    short int bits_per_sample;
    char subchunk2_id[4];
    int subchunk2_size;         // subchunk2_size denotes the number of samples.
} header;

typedef struct header_file* header_p;


short int * read_audio_samples(const char *filename, int  count[])
{
	
	errno_t err;
	short int * ptr = NULL;
	FILE * infile;


	err = fopen_s(&infile, filename, "rb");
	//err = fopen_s(&infile, "the_voice_of_peace_marked.wav", "rb");
	if (infile == NULL)
	{
		printf("read_audio_samples Error - Couldn't open WAV file  \n");
	}
	else{
		header_p meta = (header_p)malloc(sizeof(header));   // header_p points to a header struct that contains the wave file metadata fields

		int nb;                         // variable storing number of bytes returned

   
        fread(meta, 1, sizeof(header), infile);
		* count = (meta->subchunk2_size) / 2;
		ptr =  (short int *) malloc(sizeof(short int) * count[0]);

		nb = fread(ptr, sizeof(short int), count[0], infile);

	}
	return ptr;

}