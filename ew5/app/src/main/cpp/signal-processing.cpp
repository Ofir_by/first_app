//
// Created by Alon on 11-Feb-21.
//
#include <math.h>
#include <stdio.h>
#include "parameters.h"
#include "fft.h"
#include "signal-processing.h"

void copy_array(short* in, short* out, int length) {
    for (int j = 0; j < length; j++) {
        out[j] = in[j];
    }
}

void singen(short* out, int length ,short freq_L ,short freq_R, float Fs){
    short i=0;
    float stepL=0, stepR=0;
    static float phaseL=0.0, phaseR=0.0;

    stepL = ((float)freq_L)/Fs;
    stepR = ((float)freq_R)/Fs;

    for(i = 0; i < length/2 ; i++){
        out[2*i]  = (short)(Q14*sin(phaseL));
        out[2*i+1]= (short)(Q14* sin(phaseR + PI / 2));      //change to sin(phaseR+PI/2)) to creat a circle


        phaseL += stepL*2*PI;
        if(phaseL>(2*PI)) phaseL -= 2*PI;
        phaseR += stepR*2*PI;
        if(phaseR>(2*PI)) phaseR -= 2*PI;
    }
}

short sintbl[160] =
                       {0, 643, 1285, 1926, 2563,
                       3196, 3825, 4447, 5063, 5670,6270, 6859, 7438, 8005, 8560,
                       9102, 9630, 10143, 10640, 11121,11585, 12030, 12458, 12866, 13254,
                       13622, 13969, 14294, 14597, 14878,15136, 15370, 15581, 15768, 15930,
                       16068, 16181, 16269, 16332, 16370,16383, 16370, 16332, 16269, 16181,
                       16068, 15930, 15768, 15581, 15370,15136, 14878, 14597, 14294, 13969,
                       13622, 13254, 12866, 12458, 12030,11585, 11121, 10640, 10143, 9630,
                       9102, 8560, 8005, 7438, 6859,6270, 5670, 5063, 4447, 3825,
                       3196, 2563, 1926, 1285, 643,0, -643, -1285, -1926, -2563,
                       -3196, -3825, -4447, -5063, -5670,-6270, -6859, -7438, -8005, -8560,
                       -9102, -9630, -10143, -10640, -11121,-11585, -12030, -12458, -12866, -13254,
                       -13622, -13969, -14294, -14597, -14878,-15136, -15370, -15581, -15768, -15930,
                       -16068, -16181, -16269, -16332, -16370,-16383, -16370, -16332, -16269, -16181,
                       -16068, -15930, -15768, -15581, -15370,-15136, -14878, -14597, -14294, -13969,
                       -13622, -13254, -12866, -12458, -12030,-11585, -11121, -10640, -10143, -9630,
                       -9102, -8560, -8005, -7438, -6859,-6270, -5670, -5063, -4447, -3825,
                       -3196, -2563, -1926, -1285, -643};

//void singen_from_table_old(short* out, int length ,short freq_L ,short freq_R, float Fs) {
// int i;
// static short stepL, stepR, jumpL=0, jumpR=0;
// stepL=((float)freq_L);
// stepR=((float)freq_R);
// for (i=0; i<(length/2);i++){
//     jumpL= jumpL + stepL;
//     jumpR= jumpR + stepR;
//     if(jumpL>=Fs) jumpL=jumpL-Fs;
//     if(jumpR>=Fs) jumpR=jumpR-Fs;
//     out[2*i+1]= sintbl[jumpR/100];
//     out[2*i]=sintbl[jumpL/100];
// }
//}

void singen_from_table(short* out, int length ,short freq, float Fs) {
    int i;
    static short step, jump=0;
    step=freq;
    for (i=0; i<(length/2);i++){

        jump= jump + step;

        if(jump>=Fs) jump=jump-Fs;
        out[2*i+1]= sintbl[jump/100];
        out[2*i]=sintbl[jump/100];
    }
}


void tone_detection(short adin[2 * BLOCK_SIZE], double tones[], int slice) {
  /* @brief    finds the tones whith the largest amplitude within a given slice 
     @param    adin[]: the audio input
               tones[]: the detected tones from the slice
               slice: the part of the input audio array we detect from

  */
    int j, i,k, s=0;
    double amplitude[FFT_SIZE], tones_amp[DETECTED_TONES] = { 0 };
    double temp = 0, tempidx = 0;

    double SA[FFT_SIZE][2] = { 0 };

    for (j = 0; j < MIN_FFT_SIZE; j++) {
        SA[j][0] = ((double) adin[2*slice*MIN_FFT_SIZE+2 * j]);
        SA[j][1] = 0;
    }

    gpfft(SA, 0);
    
    

    for (i = 0; i < DETECTED_TONES; i++) {
        tones[i] = END_TONE*SAMPLING_FREQ / 4*FFT_SIZE;  
    }
    for (k = START_TONE/JUMP; k < START_TONE/JUMP+(END_TONE-START_TONE)/(DETECTED_TONES*JUMP) ; k++) {



        for (i = 0; i < DETECTED_TONES; i++) {
            j=(k+i*DETECTED_TONES)*JUMP;
            amplitude[j] = SA[j][0] * SA[j][0] + SA[j][1] * SA[j][1];
            if ((amplitude[j] > tones_amp[i])&& (amplitude[j] > 0.01)){  
                tones_amp[i] = amplitude[j];
                tones[i] = double(j) * SAMPLING_FREQ / FFT_SIZE;
                //printf("\n tones[%d]=%f" ,i,tones[i] );
            }
        }
    }
//    if (amplitude[j] > tones_amp[0]) {
//            tones_amp[DETECTED_TONES - 1] = amplitude[j];
//            tones[DETECTED_TONES - 1] = double(j) * SAMPLING_FREQ / FFT_SIZE;
//            for (i = 1; i < DETECTED_TONES; i++) {
//                if (tones_amp[DETECTED_TONES - i] > tones_amp[DETECTED_TONES - 1 - i]) {
//                    temp = tones_amp[DETECTED_TONES - 1 - i];
//                    tones_amp[DETECTED_TONES - 1 - i] = tones_amp[DETECTED_TONES - i];
//                    tones_amp[DETECTED_TONES - i] = temp;
//                    tempidx = tones[DETECTED_TONES - 1 - i];
//                    tones[DETECTED_TONES - 1 - i] = tones[DETECTED_TONES - i];
//                    tones[DETECTED_TONES - i] = tempidx;
//                }
//            }


   //intf("\n s= %d", s);
}



void convert_ascii_to_tone(double incoded[FFT_SIZE][2], char a) {
   /* @brief    codes a char into a set of frequencies
      @param    incoded[FFT_SIZE][2]: the array that the tone representing the char is written into
                a: the char we want to code
   */
    int val = 0;
    val = (int)a;
    int i, j, k,l;
    i = val / 64 + 12;
    j = (val % 64) / 16 + 8;
    k = (val % 16) / 4 + 4;
    l = (val % 4);

    incoded[START_TONE + JUMP*i][0] = FFT_SIZE / 8;
    incoded[FFT_SIZE - START_TONE - JUMP * i][0] = FFT_SIZE / 8;
    incoded[START_TONE + JUMP * j][0] = FFT_SIZE / 8;
    incoded[FFT_SIZE - START_TONE - JUMP * j][0] = FFT_SIZE / 8;
    incoded[START_TONE + JUMP * k][0] = FFT_SIZE / 8;
    incoded[FFT_SIZE - START_TONE - JUMP * k][0] = FFT_SIZE / 8;
    incoded[START_TONE + JUMP * l][0] = FFT_SIZE / 8;
    incoded[FFT_SIZE - START_TONE - JUMP * l][0] = FFT_SIZE / 8;
    printf("\n freqs: %d  %d  %d  %d ", START_TONE + JUMP * i, START_TONE + JUMP * j, START_TONE + JUMP * k, START_TONE + JUMP * l);
   ifft(incoded,0);

}

char convert_tone_to_ascii(double tones[]) {
   /* @brief    converts a set of frequencies to their corresponding char
      @param    tones[]: the frequencies
      @return   a: the detected char
   */
    int a = 0,  base4[4] = { 0 }, i = 0, j = 0;
    
    double temp = 0;
    for (j = 0;j < DETECTED_TONES-1;j++) {
        for (i = 0;i < DETECTED_TONES - j-1;i++) {
            if (tones[i] > tones[i + 1]) {
                temp = tones[i + 1];
                tones[i + 1] = tones[i];
                tones[i] = temp;
            }
        }
       
    }
    
    for (i = 0;i < DETECTED_TONES;i++) {
        base4[i] = (tones[i]) * FFT_SIZE / (JUMP*SAMPLING_FREQ) - START_TONE/JUMP -i*4;
        a += base4[i] * pow(4 ,i);
    }
    return (char)a;

}

void convert_double2short(short SSA[2*BLOCK_SIZE], double SA[BLOCK_SIZE][2]){
    /* @brief    inserts values from double array to short array 
       @param    SA[BLOCK_SIZE][2]: the values source
                 SSA[2*BLOCK_SIZE]: the destination
    */
    int i=0;
    for (i = 0;i < FFT_SIZE;i++) {
        SSA[2*i] = (short)(Q14*(SA[i][0]));
        SSA[2*i+1] = (short)(Q14*(SA[i][1]));
    }
}

char detect_char (short audioDataIn[2 * BLOCK_SIZE]){
    /* @brief    detects the right char from an audio signal
       @param    audioDatain[]: the audio signal 
       @return   majority_letter: the detected char
    */
    int slice=0;
    char received_letters[FFT_SIZE/MIN_FFT_SIZE] ={0};
    char majority_letter = ' ';
    double tones[DETECTED_TONES];
    for(slice=0;slice<FFT_SIZE/MIN_FFT_SIZE;slice++) {
        tone_detection(audioDataIn, tones, slice);
        received_letters[slice] = convert_tone_to_ascii(tones);
    }
    majority_letter = majority_voting(received_letters);
    return majority_letter;
}


char majority_voting( char received_letters[FFT_SIZE/MIN_FFT_SIZE]){
    /* @brief    finds the most accuring char out of a char array
       @param    received_letters[]: array of detected chars 
       @return   major_letter: the most accuring char
    */
    int i, max = 0 ;
    char major_letter=' ';
    int ascii[256]= {0};
    for (i = 0;i < FFT_SIZE / MIN_FFT_SIZE;i++) {
        ascii[int(received_letters[i])]++;
        if(ascii[int(received_letters[i])]>max){    //updating the maximum
            max = ascii[int(received_letters[i])];
            major_letter = received_letters[i];

        }
    }
   // printf("%c is the major letter " , major_letter);
    return major_letter;

};

void input_insertion(short two_blocks[4 * BLOCK_SIZE], short new_SA[2*BLOCK_SIZE]) {
    /* @brief    
       @param    
    */
    int i = 0;
    for (i = 0; i < 2*BLOCK_SIZE; i++) {
        two_blocks[i] = two_blocks[i + 2 * BLOCK_SIZE];
        two_blocks[i + 2 * BLOCK_SIZE] = new_SA[i];
    }
}