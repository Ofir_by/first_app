//
// Created by Alon on 11-Feb-21.
//

#ifndef ACOUSTICMODEM_SIGNAL_PROCESSING_H
#define ACOUSTICMODEM_SIGNAL_PROCESSING_H

void copy_array(short* in, short* out, int length);
void singen(short* out, int length ,short freq_L ,short freq_R, float Fs);
void singen_from_table(short* out, int length ,short freq, float Fs);
void tone_detection(short adin[2 * BLOCK_SIZE], double tones[],int slice);
void convert_ascii_to_tone(double incoded[FFT_SIZE][2], char a);
char convert_tone_to_ascii(double tones[]);
void convert_double2short(short SSA[2*BLOCK_SIZE], double SA[BLOCK_SIZE][2]);
char majority_voting( char received_letters[FFT_SIZE/MIN_FFT_SIZE]);
char detect_char (short audioDataIn[2 * BLOCK_SIZE]);
void input_insertion(short two_blocks[4 * BLOCK_SIZE], short new_SA[2 * BLOCK_SIZE]);
#endif //ACOUSTICMODEM_SIGNAL_PROCESSING_H
