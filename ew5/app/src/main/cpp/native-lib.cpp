#include <jni.h>
#include <string>
#include <sstream>
#include "parameters.h"
#include "signal-processing.h"
#include "fft.h"

extern "C" JNIEXPORT jstring JNICALL
Java_e_ewallet_MainActivity_stringFromJNI(JNIEnv* env, jobject /* this */) {
    std::string hello = "Audio Loopback Demo";
    return env->NewStringUTF(hello.c_str());
}



extern "C"
JNIEXPORT void JNICALL
Java_e_ewallet_MainActivity_AudioProcessing(JNIEnv *env, jobject thiz,
                                            jshortArray audio_data_in,
                                            jshortArray audio_data_out,
                                            jint fs,
                                            jchar message
) {
/* @brief    generates audio signal that represents a given char
   @param    audio_data_in[]: the recorded audio
             audio_data_out[]: the generated audio
             message: the char we want to code into a signal
*/
    jshort *audioDataIn;
    jshort *audioDataOut;
    audioDataIn = env->GetShortArrayElements(audio_data_in, NULL);
    audioDataOut = env->GetShortArrayElements(audio_data_out, NULL);
    int length = env->GetArrayLength(audio_data_in);
    double SA[BLOCK_SIZE][2]={0};


        convert_ascii_to_tone(SA, message);
        convert_double2short(audioDataOut,SA);


    env->ReleaseShortArrayElements(audio_data_in, audioDataIn, 0);
    env->ReleaseShortArrayElements(audio_data_out, audioDataOut, 0);
}
extern "C"
JNIEXPORT jchar JNICALL
Java_e_ewallet_MainActivity_Tone2Char(JNIEnv *env, jobject thiz,
                                            jshortArray audio_data_in,
                                            jint block_num,
                                            jint fs
) {
    /* @brief    decodes the char from an audio input
       @param    audio_data_in[]: the audio input
       @return   detected_char: the detected char
  */
    jshort *audioDataIn;

    audioDataIn = env->GetShortArrayElements(audio_data_in, NULL);
    char detected_char =detect_char(audioDataIn);
    env->ReleaseShortArrayElements(audio_data_in, audioDataIn, 0);
    return detected_char;


}
extern "C"
JNIEXPORT void JNICALL
Java_e_ewallet_MainActivity_initialize(JNIEnv *env, jobject thiz) {
    fft_init(MAX_FFT_SIZE);
}