package e.ewallet;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.os.Process;

import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends AppCompatActivity {
    private static final int INPUT_CHANNELS = 2, SAMPLING_RATE = 16000, BUFFER_SIZE = 8192;
    private static char  END =3, START =2;
    public static final int REQUEST_AUDIO_PERMISSION_CODE = 1;
    static int inputSampleRate = SAMPLING_RATE;
    static int send_num =-1, msg_size=0,receive_num=-1;
    static AudioRecord mAudioRecord;
    static AudioTrack audioTrack;
    static AudioManager m_amAudioManager;
    static short[] mSamplesShort = new short[BUFFER_SIZE / 2 * INPUT_CHANNELS];
    static short[] mSamplesShortOut = new short[BUFFER_SIZE];
    private ScheduledExecutorService scheduleTaskExecutor;
    static private TextView  ReportStatus, HeaderMsg;
    static String StatusMsg=" ", SystemMsg="SysMsg",str="No message detected.";
    static String def_str="No message detected.";
    static char[] msg;
    static char end = END, start = START, letter;
    private Button Measurebutn;
    private Button Sendbutn;
    private static final boolean ACTIVE = true, STOPPED = false;
    private boolean ActiveStopped=STOPPED;

    private Switch DetectionSwitch;
    private static final boolean FromDevice = false, FromOut = true;
    private EditText InputTextbox;
    static String InputMessage;



    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        scheduleTaskExecutor= Executors.newScheduledThreadPool(5);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);
        final Context context = getApplicationContext();

        getWindow().getDecorView().setBackgroundColor(Color.BLACK);
        ReportStatus = findViewById(R.id.internals);
        HeaderMsg = findViewById(R.id.Distance);

        scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {


            //TextView SysMsg = findViewById(R.id.SysMsg);

            public void run() {
                // runOnUiThread to update UI:
                runOnUiThread(new Runnable() {
                    public void run() {

                        ReportStatus.setText(StatusMsg);

                                HeaderMsg.setText(receive_num==-1?def_str:str);




                        //Log.w("Main/", "UI Thread Running");
                    }
                });
            }
        }, 0, 100, TimeUnit.MILLISECONDS);

        Measurebutn = (Button) findViewById(R.id.Measure);
        Measurebutn.setBackgroundColor(Color.GREEN);
        Measurebutn.setText(R.string.Start_Measure);
        Measurebutn.setEnabled(true);

        Sendbutn=(Button) findViewById(R.id.send);
        Sendbutn.setBackgroundColor(getResources().getColor(R.color.main_blue));
        Sendbutn.setText(R.string.Send_Button);
        Sendbutn.setTextColor(getResources().getColor(R.color.blue_dark));
        //Sendbutn.setTextColor(Color.WHITE);
        Sendbutn.setEnabled(true);

        DetectionSwitch = (Switch) findViewById(R.id.switch1);
        //DetectionSwitch.setBackgroundColor(Color.MAGENTA);
        DetectionSwitch.setTextColor(getResources().getColor(R.color.purple_200));
        DetectionSwitch.setChecked(FromDevice);

        InputTextbox = (EditText) findViewById(R.id.InputMessage);
        InputTextbox.setBackgroundColor(Color.LTGRAY);
        InputTextbox.setTextColor(Color.BLACK);

        initialize();

        while (!CheckPermissions()) {
            //Log.w("MainActivity/", "Requesting Audio Permissions");
            RequestPermissions();
        }

        if (CheckPermissions()) {
            Log.w("Main/", "Audio Permissions Granted");
            StatusMsg = String.format("Audio Permissions Granted");
            RunAudioThread();
        } else{
            Log.w("Main/", "Audio Permissions Denied");
        }

        Measurebutn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ActiveStopped) {
                    Measurebutn.setText(R.string.Start_Measure);
                    Measurebutn.setBackgroundColor(Color.GREEN);
                    ActiveStopped = STOPPED;
                }else{
                    Measurebutn.setText(R.string.Stop_Measure);
                    Measurebutn.setBackgroundColor(Color.RED);
                    ActiveStopped = ACTIVE;
                }

            }
        });
        Sendbutn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMessage = start+ (InputTextbox.getText().toString())+end;
                msg=InputMessage.toCharArray();
                msg_size = InputMessage.length();
                send_num =0;

            }
        });
        DetectionSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DetectionSwitch.isChecked()) {
                    DetectionSwitch.setText(R.string.Device);
                } else {
                    DetectionSwitch.setText(R.string.Outside);
                }
            }
        });

    }

    public boolean CheckPermissions() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }
    


    private void RequestPermissions() {
        ActivityCompat.requestPermissions(MainActivity.this, new String[]{RECORD_AUDIO, WRITE_EXTERNAL_STORAGE}, REQUEST_AUDIO_PERMISSION_CODE);
    }

    public void RunAudioThread(){
        Log.w("Main/RunAudioThread", "Starting");
        new Thread(new Runnable() {
            @Override
            public void run() {
                Process.setThreadPriority(Process.THREAD_PRIORITY_URGENT_AUDIO);
                StartAudioIO();

                while (mAudioRecord != null && mAudioRecord.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING) {
                    mAudioRecord.read(mSamplesShort, 0, (BUFFER_SIZE / 2) * INPUT_CHANNELS);

                     if(send_num <msg_size  && send_num >=0) {
                        AudioProcessing(mSamplesShort, mSamplesShortOut, SAMPLING_RATE, msg[send_num]);
                         send_num++;
                    }else if(send_num ==msg_size){
                         send_num = -1;
                         Arrays.fill(mSamplesShortOut, (short) 0);
                     }
                    letter = Tone2Char (DetectionSwitch.isChecked()? mSamplesShortOut:mSamplesShort, send_num, SAMPLING_RATE);
                     if(letter==(char)START){
                        receive_num=0;
                        str = "Received message: ";
                    }else if (receive_num==0 && letter!=(char)END){
                        str+=letter;
                    }else{
                        receive_num=1;
                    }


                 if (ActiveStopped == STOPPED){
                        for (int i = 0; i < BUFFER_SIZE; i++) {
                            mSamplesShortOut[i] = 0;
                        }
                  }
                  audioTrack.write(mSamplesShortOut, 0, (BUFFER_SIZE / 2) * INPUT_CHANNELS);
                }
                Log.w("Main/RunAudioThread", "End");
            }
        }).start();
    }

    public void StartAudioIO() {
        Log.w("Main/StartAudioIO", "Starting");

        int inputChannel=INPUT_CHANNELS;

        mAudioRecord = new AudioRecord(MediaRecorder.AudioSource.CAMCORDER,
                inputSampleRate, inputChannel == 1 ? AudioFormat.CHANNEL_IN_MONO : AudioFormat.CHANNEL_IN_STEREO,
                AudioFormat.ENCODING_PCM_16BIT, BUFFER_SIZE * inputChannel);

        mAudioRecord.startRecording();

        m_amAudioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);

        audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
                inputSampleRate, AudioFormat.CHANNEL_IN_STEREO,
                AudioFormat.ENCODING_PCM_16BIT, BUFFER_SIZE * 2,
                AudioTrack.MODE_STREAM);

        audioTrack.setStereoVolume((float)0.9, (float)0.9);

        audioTrack.play();
    }

    public void StopAudioIO() {
        try {
            mAudioRecord.stop();
            mAudioRecord.release();
            audioTrack.stop();
            audioTrack.release();
            Log.w("Main/StopAudioIO", "Audio IO Stopped");
        } catch (IllegalStateException e) {
            Log.w("Main/StopAudioIO", "FAILED Audio IO Stopping");
            e.printStackTrace();
        }
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();

    public native void AudioProcessing(short[] audioDataIn, short[] audioDataOut, int fs, char msg);
    public native char Tone2Char(short[] audioDataIn, int block_num, int fs);
    public native void initialize ();
}
