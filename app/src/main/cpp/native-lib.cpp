#include <jni.h>
#include <string>
#include <iostream>
using namespace std;


void copy_array(jshort *pInt, jshort *pInt1, int length);


void Tone(jshort *pInt, jshort *i, int length);

void singen1(jshort *pInt, jshort *pInt1, jshort l, jshort r, jfloat d);

extern "C" JNIEXPORT jstring JNICALL
Java_com_example_myapplication_MainActivity_stringFromJNI(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "AudioProcessing()";
    return env->NewStringUTF(hello.c_str());
}

void singen1(jshort *pInt, jshort *pInt1, jshort l, jshort r, jfloat d);


void tone_detection(jshort *pInt, jshort *pInt1, int length);

extern "C"
JNIEXPORT void JNICALL
Java_com_example_myapplication_MainActivity_AudioProcessing(JNIEnv *env, jobject thiz,
                                                            jshortArray audio_data_in,
                                                            jshortArray audio_data_out) {
    jshort *audioDataIn;
    jshort *audioDataOut;
    audioDataIn = env->GetShortArrayElements(audio_data_in, NULL);
    audioDataOut = env->GetShortArrayElements(audio_data_out, NULL);
    int length = env->GetArrayLength(audio_data_in);
    // TODO: implement AudioProcessing()
     copy_array(audioDataIn, audioDataOut, length);
    //tone_detection(audioDataIn, audioDataOut, 0);
    env->ReleaseShortArrayElements(audio_data_in, audioDataIn, 0);
    env->ReleaseShortArrayElements(audio_data_out, audioDataOut, 0);
}





/*
extern "C"
JNIEXPORT void JNICALL
Java_com_example_myapplication_MainActivity_CreateTone(JNIEnv *env, jobject thiz,
                                                       jshortArray audio_data_out, jshort sin__l,
                                                       jshort sin__r) {
    jshort *audioDataOut_R;
    jshort *audioDataOut_L;
    audioDataOut_R = env->GetShortArrayElements(audio_data_out, NULL);
    audioDataOut_L = env->GetShortArrayElements(audio_data_out, NULL);
    // TODO: implement CreateTone()
    singen1(audioDataOut_L, audioDataOut_R,sin__l, sin__r);
    env->ReleaseShortArrayElements(audio_data_out, audioDataOut_R, 0);
    env->ReleaseShortArrayElements(audio_data_out, audioDataOut_L, 0);

}*/
extern "C"
JNIEXPORT void JNICALL
Java_com_example_myapplication_MainActivity_CreateTone(JNIEnv *env, jobject thiz,
                                                       jshortArray audio_data_out, jint sin__l,
                                                       jint sin__r, jfloat fs) {
    jshort *audioDataOut_R;
    jshort *audioDataOut_L;
    audioDataOut_R = env->GetShortArrayElements(audio_data_out, NULL);
    audioDataOut_L = env->GetShortArrayElements(audio_data_out, NULL);
    // TODO: implement CreateTone()
    singen1(audioDataOut_L, audioDataOut_R, sin__l, sin__r, fs);
    env->ReleaseShortArrayElements(audio_data_out, audioDataOut_R, 0);
    env->ReleaseShortArrayElements(audio_data_out, audioDataOut_L, 0);
}
