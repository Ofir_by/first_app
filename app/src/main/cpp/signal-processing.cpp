//
// Created by Ofir Ben Yosef on 08/09/2021.
//
#include "stdio.h"
#include "signal-processing.h"
#include <iostream>
#include <math.h>
#include <signal.h>
#include "parameters.h"
#include "fft.h"
// define
#define BUFCOUNT  8192


using namespace std;
void copy_array(short* in, short* out, int length) {
    for (int j = 0; j < length; j++) {
        out[j] = in[j];
        printf("hello\n");
    }

}

void singen1(short l[], short r[], short sin_L, short sin_R, float fs) {
    short i=0;
    float stepL=0, stepR=0;
    static float phaseL=0.0, phaseR=0.0;

    stepL = ((float)sin_L)/fs;
    stepR = ((float)sin_R)/fs;

    for(i = 0; i < BUFCOUNT/2 ; i++){
        l[2*i]  = (short)(Q14*sin(phaseL));
        l[2*i+1]= (short)(Q14* sin(phaseR + PI / 2));  //change to sin(phaseR+PI/2)) to creat a circle

        phaseL += stepL*2*PI;
        if(phaseL>(2*PI)) phaseL -= 2*PI;
        phaseR += stepR*2*PI;
        if(phaseR>(2*PI)) phaseR -= 2*PI;
    }
}

void tone_detection(short adin[2 * BLOCK_SIZE], double tones[], int slice) {
    /* @brief    finds the tones whith the largest amplitude within a given slice
       @param    adin[]: the audio input
                 tones[]: the detected tones from the slice
                 slice: the part of the input audio array we detect from
    */
    int j, i,k, s=0;
    double amplitude[FFT_SIZE], tones_amp[DETECTED_TONES] = { 0 };
    double temp = 0, tempidx = 0;

    double SA[FFT_SIZE][2] = { 0 };

    for (j = 0; j < MIN_FFT_SIZE; j++) {
        SA[j][0] = ((double) adin[2*slice*MIN_FFT_SIZE+2 * j]);
        SA[j][1] = 0;
    }

    gpfft(SA, 0);
    for (i = 0; i < DETECTED_TONES; i++) {
        tones[i] = END_TONE*SAMPLING_FREQ / 4*FFT_SIZE;
    }
    for (k = START_TONE/JUMP; k < START_TONE/JUMP+(END_TONE-START_TONE)/(DETECTED_TONES*JUMP) ; k++) {



        for (i = 0; i < DETECTED_TONES; i++) {
            j=(k+i*DETECTED_TONES)*JUMP;
            amplitude[j] = SA[j][0] * SA[j][0] + SA[j][1] * SA[j][1];
            if ((amplitude[j] > tones_amp[i])&& (amplitude[j] > 0.01)){
                tones_amp[i] = amplitude[j];
                tones[i] = double(j) * SAMPLING_FREQ / FFT_SIZE;
                printf("\n tones[%d]=%f" ,i,tones[i] );
            }
        }
    }
//    if (amplitude[j] > tones_amp[0]) {
//            tones_amp[DETECTED_TONES - 1] = amplitude[j];
//            tones[DETECTED_TONES - 1] = double(j) * SAMPLING_FREQ / FFT_SIZE;
//            for (i = 1; i < DETECTED_TONES; i++) {
//                if (tones_amp[DETECTED_TONES - i] > tones_amp[DETECTED_TONES - 1 - i]) {
//                    temp = tones_amp[DETECTED_TONES - 1 - i];
//                    tones_amp[DETECTED_TONES - 1 - i] = tones_amp[DETECTED_TONES - i];
//                    tones_amp[DETECTED_TONES - i] = temp;
//                    tempidx = tones[DETECTED_TONES - 1 - i];
//                    tones[DETECTED_TONES - 1 - i] = tones[DETECTED_TONES - i];
//                    tones[DETECTED_TONES - i] = tempidx;
//                }
//            }


    //intf("\n s= %d", s);
}
void convert_ascii_to_tone(double incoded[FFT_SIZE][2], char a) {
    /* @brief    codes a char into a set of frequencies
       @param    incoded[FFT_SIZE][2]: the array that the tone representing the char is written into
                 a: the char we want to code
    */
    int val = 0;
    val = (int)a;
    int i, j, k,l;
    i = val / 64 + 12;
    j = (val % 64) / 16 + 8;
    k = (val % 16) / 4 + 4;
    l = (val % 4);

    incoded[START_TONE + JUMP*i][0] = FFT_SIZE / 8;
    incoded[FFT_SIZE - START_TONE - JUMP * i][0] = FFT_SIZE / 8;
    incoded[START_TONE + JUMP * j][0] = FFT_SIZE / 8;
    incoded[FFT_SIZE - START_TONE - JUMP * j][0] = FFT_SIZE / 8;
    incoded[START_TONE + JUMP * k][0] = FFT_SIZE / 8;
    incoded[FFT_SIZE - START_TONE - JUMP * k][0] = FFT_SIZE / 8;
    incoded[START_TONE + JUMP * l][0] = FFT_SIZE / 8;
    incoded[FFT_SIZE - START_TONE - JUMP * l][0] = FFT_SIZE / 8;
    printf("\n freqs: %d  %d  %d  %d ", START_TONE + JUMP * i, START_TONE + JUMP * j, START_TONE + JUMP * k, START_TONE + JUMP * l);
    ifft(incoded,0);

}

char convert_tone_to_ascii(double tones[]) {
    /* @brief    converts a set of frequencies to their corresponding char
       @param    tones[]: the frequencies
       @return   a: the detected char
    */
    int a = 0,  base4[4] = { 0 }, i = 0, j = 0;

    double temp = 0;
    for (j = 0;j < DETECTED_TONES-1;j++) {
        for (i = 0;i < DETECTED_TONES - j-1;i++) {
            if (tones[i] > tones[i + 1]) {
                temp = tones[i + 1];
                tones[i + 1] = tones[i];
                tones[i] = temp;
            }
        }

    }

    for (i = 0;i < DETECTED_TONES;i++) {
        base4[i] = (tones[i]) * FFT_SIZE / (JUMP*SAMPLING_FREQ) - START_TONE/JUMP -i*4;
        a += base4[i] * pow(4 ,i);
    }
    return (char)a;

}

void convert_double2short(short SSA[2*BLOCK_SIZE], double SA[BLOCK_SIZE][2]){
    /* @brief    inserts values from double array to short array
       @param    SA[BLOCK_SIZE][2]: the values source
                 SSA[2*BLOCK_SIZE]: the destination
    */
    int i=0;
    for (i = 0;i < FFT_SIZE;i++) {
        SSA[2*i] = (short)(Q14*(SA[i][0]));
        SSA[2*i+1] = (short)(Q14*(SA[i][1]));
    }
}
